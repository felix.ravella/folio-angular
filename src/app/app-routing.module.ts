import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageParcoursComponent } from './page-parcours/page-parcours.component';
import { PageProjetsComponent } from './page-projets/page-projets.component';
import { PageAccueilComponent } from './page-accueil/page-accueil.component';

const routes: Routes = [
  {path: 'parcours', component: PageParcoursComponent},
  {path: 'projets', component: PageProjetsComponent},
  {path: '', component: PageAccueilComponent},
  {path: '**', redirectTo: '', pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
