import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageParcoursComponent } from './page-parcours.component';

describe('PageParcoursComponent', () => {
  let component: PageParcoursComponent;
  let fixture: ComponentFixture<PageParcoursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageParcoursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageParcoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
